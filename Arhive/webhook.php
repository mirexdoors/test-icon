<?php


//Определение переменных
$filename = 'main.log';
$curentTime = date("H:i:s");

//Разбор запроса
$method = $_SERVER['REQUEST_METHOD'];
$postData = file_get_contents('php://input');

// echo PHP_EOL .  '$postData: ' . $postData;

//Декодирование
$data = json_decode($postData, true); //Это ассоциативный массив

// echo PHP_EOL . "После json_decode: " . $postData;

$text = PHP_EOL . "Запись: " . $curentTime . "\n";
$text .= PHP_EOL . " Метод " . $method . "\n";
$text .= PHP_EOL . " SERVER_ADDR " . $_SERVER['SERVER_ADDR'] . "\n";
$text .= PHP_EOL . " argv " . $_SERVER['argv'] . "\n";
$text .= PHP_EOL . " QUERY_STRING " . $_SERVER['QUERY_STRING'] . "\n";

// print_r ($_SERVER);

// echo PHP_EOL . "До записи в файл";

file_put_contents($filename, PHP_EOL . $text, FILE_APPEND);//запись в лог файл


//Выборка нужных данных по переменным
$dateTmp = $data['comment']['updated_on'];
// $commitDate = $dateTmp;

$da = new DateTime("2018-07-17T11:02:48+03:00");
echo $da->format('d-m-Y H:i:s');
// $commitDate = new DateTime($data['comment']['updated_on'])->format('Y-m-d H:i:s');
// date_format($dateTmp, "d.m.Y H:i");//Дата и время комментария
$repositoryName = $data['repository']['full_name'];//Название репозитория
$repositoryHref = $data['repository']['links']['html']['href']; //Адрес репозитория
$commentAutor = $data['actor']['display_name']; //Имя автора
$commitText = $data['commit']['message'];//Текст комментария

//Формирование строки
$text .= $dateTmp."\n";
$text .= $commentAutor."\n";
$text .= $repositoryName."\n";
$text .= $repositoryHref."\n";
$text .= $commitText."\n";

// echo $text;

//$lengthArr = count($data);



//1 Формируем массив для переменных, которые будут переданы с запросом
$tmpQueryData = array(  'POST_TITLE' => $commentAutor." закомитил в ".$repositoryName,
                        'POST_MESSAGE' => $commitText."\n".$repositoryHref);

//2 Преобразуем массив в URL-кодированную строку
$queryData = http_build_query($tmpQueryData);

//Формируем запросы
$queryUrl = 'https://icon.bitrix24.ru/rest/1/afswmgs00c5qr8y8/log.blogpost.add';

//Пишем в файл
$myQuery = $queryUrl."?".$queryData;
//file_put_contents($filename, $myQuery."\n\n", FILE_APPEND);

// Пишем в URL - отправляем запрос 
//file_put_contents($myQuery, "\n\n");

// $result = file_get_contents($myQuery);
// file_put_contents($filename, "\n"."Результат запроса:"."\n".$result."\n", FILE_APPEND);

//Пишем ответ в файл
//file_put_contents($filename, "\n"."Результат запроса:"."\n".$result."\n", FILE_APPEND);

?>
