<?
$bitrixToken = "usoya35gacg3490esyd473zdok6r6i12";
$rawPostData = file_get_contents('php://input');

if (strlen($rawPostData) > 0 || isset($_REQUEST)) {
    require_once 'vendor/autoload.php';

    ///по умолчанию - bitbucket
    $action = 'inputBitbucketWebhook';
    if ($rawPostData) {
        $data = json_decode($rawPostData, true);
    } else {
        $data = $_REQUEST;
    }

    if ($_REQUEST['event'] == 'ONTASKCOMMENTADD' && $_REQUEST['auth']['application_token'] == $bitrixToken) $action = "outputBXWebhook";
    if (!empty($data)) {
        if ($_REQUEST['auth']['domain'] == 'icon.bitrix24.ru') {//хук с битрикс24
            $taskId = $_REQUEST['data']['FIELDS_AFTER']['ID'];
            $action = 'outputBXWebhook';
        } elseif ($data['action'] == 'inputOmnidesk') { //хук с Omnidesk
            $action = $data['action'];
        }
    }

    $webhook = new Webhook2;
    $webhook->writeLog($_REQUEST);

    switch ($action) {
        //case 'inputBitbucketWebhook': $webhook->sendMessage2CRM($data);	break;
        case 'outputBXWebhook':
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/logs/query.log', PHP_EOL . print_r($_REQUEST,1), FILE_APPEND);
            //$webhook->createTaskInDesk($taskId);
            break;
        case 'inputOmnidesk':
            $webhook->createCommentBX($data);
            break;
    }

} else {
    //TODO Вывод представления из БД
}
?>
