<?

class Omnidesk {

        // нааходит сделку по id
    public function sendOmni($id){
        $url = 'https://icon.bitrix24.ru/rest/1678/6meeu7siq6mi90r2/task.item.getdata?TASK_ID='.$id;
        $curl = curl_init($url);
        $result = curl_exec($curl);
        curl_close($curl);

        $fileName="logs/all.log";
        file_put_contents($fileName, PHP_EOL . "Данные по задаче:" . PHP_EOL . print_r($result, 1), FILE_APPEND);

        // здесь нужно найти поле с тикетом(номер обращения) и отправить его в Omnidesk, вместе с $id задачи

    }
        // отправляет id задачи в omnidesk
    protected function sendIdOmni($idRecourse, $idTask) {
        $url = 'https://[domain].omnidesk.ru/api/cases/'.$idRecourse.'.json';
        $idRecourseSend = {
            "case":{
                "custom_fields":{
                    "custom_field_2389":$idTask
                }
            }
        };
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($idRecourseSend));

        $result = curl_exec($ch);

        curl_close($curl);
    }
}