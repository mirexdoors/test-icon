<?

class Webhook
{
	protected $filename  = "/logs/main.log";
	// пример webhook: https://icon.bitrix24.ru/rest/1678/5hvz0nu40expio27/profile/
	protected $crmUrl = "https://icon.bitrix24.ru/rest/1678/5hvz0nu40expio27/log.blogpost.add.json";
	protected $curentTime;
	protected $method;

	public function __construct()
	{
		$this->curentTime = date("Y-m-d H:i:s");
		$this->method = $_SERVER['REQUEST_METHOD'];
	}

	protected function encodeData($data)
	{
		return json_encode($data);
	}

	protected function createLogText($data)
	{
		$text = PHP_EOL . "Запись: " . $this->curentTime . "\n";
		$text .= PHP_EOL . " Метод " . $this->method . "\n";
		$text .= PHP_EOL . " SERVER_ADDR " . $_SERVER['SERVER_ADDR'] . "\n";
		$text .= PHP_EOL . " QUERY_STRING " . $_SERVER['QUERY_STRING'] . "\n";
		$text .= $this->encodeData($data);
		return $text;
	}

	protected function buildMessageForCRM($data)
	{
		$author = '';
		$repositoryName = '';
		$result = array();
		$commitsAmount = '';

		$dataArray = json_decode($this->encodeData($data),true);
		$repositoryLink =  $dataArray['repository']['links']['html']['href'];
		$message = 'Репозиторий: ' . $repositoryLink . "\n";

		if (isset($dataArray['push'])) {
			$author = $dataArray['push']['changes'][0]['old']['target']['author']['user']['display_name'];
			$repositoryName = $dataArray['repository']['name'];
			$commitsAmount = count($dataArray['push']['changes'][0]['commits']);
				
			foreach ($dataArray['push']['changes'][0]['commits'] as $commit) {
				$date = new DateTime($commit['date']);
				$message .= $date->format('d.m.Y H:i') . ': ';
				$message .= $commit['message'] . "\n";
				$message .= 'Автор: ' . $commit['author']['raw'] . "\n";
				$message .= 'Ссылка на коммит: ' . $commit['links']['html']['href'] . "\n";
			}

			$result = array(
				'POST_TITLE' => "В  " . $repositoryName . ' добавлен(о) ' . $commitsAmount . ' коммит(а)',
				'POST_MESSAGE' => $message
			);

		} elseif (isset($dataArray['comment'])) {
				$author = $dataArray['comment']['user']['display_name'];
				$repositoryName =  $dataArray['repository']['full_name'];
				$commitBase = $dataArray['commit']['message'];

				$date = new DateTime($dataArray['comment']['created_on']);
				$message .=  $date->format('d.m.Y H:i') . ': ';		
				$message = $dataArray['comment']['content']['raw'] . "\n";
				$message .= $dataArray['comment']['links']['html']['href'];
				$result = array(
					'POST_TITLE' => $author . " добавил в  " . $repositoryName . '  комментарий в коммит ' . $commitBase,
					'POST_MESSAGE' => $message
				);
		}

		return $result;
	}

	public function sendMessage2CRM($data)
	{
		$message = $this->buildMessageForCRM($data);
		$queryData = http_build_query($message);
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $this->crmUrl,
			CURLOPT_POSTFIELDS => $queryData,
		));

		$result = curl_exec($curl);
		curl_close($curl);
		$text = 'URL: ' . $this->crmUrl . ';  QUERY: ' . $queryData;
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/logs/query.log', PHP_EOL .  $text, FILE_APPEND);
		return true;
	}

	public  function writeLog($data)
	{
		$text = $this->createLogText($data);
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . $this->filename, PHP_EOL .  $text, FILE_APPEND);
	}


}
?>
