<?

class Webhook2
{
    const TOKEN = '3767af355d9915e30813f1e66';

    protected $crmBlogPostUrl = "https://icon.bitrix24.ru/rest/1678/5hvz0nu40expio27/log.blogpost.add.json";
    protected $crmTaskInfoUrl = "https://icon.bitrix24.ru/rest/1678/7rjjhp4q21mz4kto/task.item.getdata.json?TASKID=";
    protected $crmTaskAddCommentUrl = "https://icon.bitrix24.ru/rest/1678/7rjjhp4q21mz4kto/task.commentitem.add.json";

    protected $omnideskGetUserUrl = "https://help.iconweb.ru/api/users/";
    protected $omnideskGetCaseUrl = "https://help.iconweb.ru/api/cases/";
    protected $omnideskGetStaffUrl = "https://help.iconweb.ru/api/staff/";

    protected $filename = "/logs/main.log";
    protected $curentTime;
    protected $method;


    public function __construct()
    {
        $this->curentTime = date("Y-m-d H:i:s");
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    protected function encodeData($data)
    {
        return json_encode($data);
    }

    protected function createLogText($data)
    {
        $text = PHP_EOL . "Запись: " . $this->curentTime . "\n";
        $text .= PHP_EOL . " Метод " . $this->method . "\n";
        $text .= PHP_EOL . " SERVER_ADDR " . $_SERVER['SERVER_ADDR'] . "\n";
        $text .= PHP_EOL . " QUERY_STRING " . $_SERVER['QUERY_STRING'] . "\n";
        $text .= $this->encodeData($data);
        return $text;
    }

    protected function buildMessageForCRM($data)
    {
        $author = '';
        $repositoryName = '';
        $result = array();
        $commitsAmount = '';

        $dataArray = json_decode($this->encodeData($data), true);
        $repositoryLink = $dataArray['repository']['links']['html']['href'];
        $message = 'Репозиторий: ' . $repositoryLink . "\n";

        if (isset($dataArray['push'])) {
            $author = $dataArray['push']['changes'][0]['old']['target']['author']['user']['display_name'];
            $repositoryName = $dataArray['repository']['name'];
            $commitsAmount = count($dataArray['push']['changes'][0]['commits']);

            foreach ($dataArray['push']['changes'][0]['commits'] as $commit) {
                $date = new DateTime($commit['date']);
                $message .= $date->format('d.m.Y H:i') . ': ';
                $message .= $commit['message'] . "\n";
                $message .= 'Автор: ' . $commit['author']['raw'] . "\n";
                $message .= 'Ссылка на коммит: ' . $commit['links']['html']['href'] . "\n";
            }

            $result = array(
                'POST_TITLE' => "В  " . $repositoryName . ' добавлен(о) ' . $commitsAmount . ' коммит(а)',
                'POST_MESSAGE' => $message
            );

        } elseif (isset($dataArray['comment'])) {
            $author = $dataArray['comment']['user']['display_name'];
            $repositoryName = $dataArray['repository']['full_name'];
            $commitBase = $dataArray['commit']['message'];

            $date = new DateTime($dataArray['comment']['created_on']);
            $message .= $date->format('d.m.Y H:i') . ': ';
            $message .= $dataArray['comment']['content']['raw'] . "\n";
            $message .= $dataArray['comment']['links']['html']['href'];
            $result = array(
                'POST_TITLE' => $author . " добавил в  " . $repositoryName . '  комментарий в коммит ' . $commitBase,
                'POST_MESSAGE' => $message
            );
        }

        return $result;
    }

    public function sendMessage2CRM($data)
    {
        $message = $this->buildMessageForCRM($data);
        $queryData = http_build_query($message);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->crmBlogPostUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        return true;
    }

    public function writeLog($data)
    {
        $text = $this->createLogText($data);
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $this->filename, PHP_EOL . $text, FILE_APPEND);
    }

    protected function getTicketId($taskId)
    {
        $appRequestUrl = $this->crmTaskInfoUrl . $taskId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $appRequestUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $out = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
        if ($info['http_code'] == 200) {
            $jsonResponseArr = json_decode($out, 1);
            $ticketId = $jsonResponseArr['result']['UF_AUTO_363633899917'];
            return $ticketId;
        } else {
            return false;
        }
    }

    public function createTaskInDesk($taskId)
    {
        $ticketId = $this->getTicketId($taskId);
        if ($ticketId) {
            $url = $this->omniDeskUrl . $ticketId . '.json';
            $idRecourseSend = '{
			"case":{
				"custom_fields":{
					"custom_field_2389":' . $taskId .
                '}
				}
			}';

            $queryData = http_build_query($idRecourseSend);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $queryData);
            $result = curl_exec($ch);
            curl_close($ch);
            $text = 'URL: ' . $this->crmBlogPostUrl . ';  QUERY: ' . $queryData;
            $text .= $result;
        } else {
            return false;
        }
    }

    protected function getTaskStatus($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_USERPWD => "info@iconweb.ru:" . self::TOKEN,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER, array('Content-Type: application/json'),
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->omnideskGetCaseUrl . $id . '.json'
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    protected function getUserNameById($id)
    {
        $case = json_decode($this->getTaskStatus($id), true);
        if ($case['case']['status'] == 'waiting')  //ответил сотрудник
        {
            $senderId = $case['case']['staff_id'];
            $url = $this->omnideskGetStaffUrl . $senderId . '.json';
        } elseif ($case['case']['status'] == 'open') {
            $senderId = $case['case']['user_id'];
            $url = $this->omnideskGetUserUrl . $senderId . '.json';
        }


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_USERPWD => "info@iconweb.ru:" . self::TOKEN,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER, array('Content-Type: application/json'),
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, true);
        foreach ($result as $userType => $userInfo)
        {
            if ($userType == 'staff') {
                $name = $userInfo['staff_full_name'];
            } elseif ($userType == 'user') {
                $name = $userInfo['user_full_name'];
            }
        }

        return $name;

    }

    public function createCommentBX($messageData)
    {

        $userName = $this->getUserNameById($messageData['caseId']);


        $params = array(
            "TASKID" => $messageData['taskId'],
            "FIELDS" => array(
                "POST_MESSAGE" => $userName . ' добавил новый комментарий в обращение ' . $messageData['link'] . ' :' . PHP_EOL . $messageData['message']
            )
        );
        $queryData = http_build_query($params);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->crmTaskAddCommentUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        $text = 'URL: ' . $this->crmTaskAddCommentUrl . ';  BODY: ' . $queryData;
        return true;
    }
}

?>
